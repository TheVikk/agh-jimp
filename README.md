# Projekt Agh JiMP2 - Arkadiusz Pasek

# About
The goal of the game is to score as high as possible, while avoiding obstacles & not touching the ground.
## Screenshots
![1](readMeImages/r2.JPG) 
![2](readMeImages/r4.JPG)
![3](readMeImages/r3.JPG)

# IDE
Project has been build using Microsoft Visual Studio Community 2017
#### IDE Setup
* Add SFML/include folder to "Include Directories" in Project Properties
* Add SFML/lib folder to "Library Directories" in Project Properties
* Add "SFML_STATIC" to Preprocessor Definitions in Project Properties
* Add Additional Dependencies:
	* sfml-graphics-s.lib
	* sfml-window-s.lib
	* sfml-system-s.lib
	* opengl32.lib
	* freetype.lib
	* winmm.lib
	* gdi32.lib

# Unit Testing
Unit testing framework used: Google Test (gtest)

# Libaries and Target
The Project uses SFML as main 2D graphics library, it has been created to run only on Windows OS, but it can be built for mobile.
# Copyrights
Any music or images used in the project have been licensed as free for commercial use.
### Here they are:  
* https://pixabay.com/photos/nature-landscape-mountains-sky-3616194/
* https://www.freepik.com/free-vector/hand-drawn-bird-collection_1564558.htm