#include "pch.h"

//#include <SFML/Graphics.hpp>

#include "../Application/Player.h"

TEST(TestCaseName, TestName) 
{
  EXPECT_EQ(1, 1);
  EXPECT_TRUE(true);
}

TEST(PlayerCreationTest, playerCanBeCreated)
{
	Player player;
}

TEST(RectanglesTest, rectanglesResetCorrectlyWhenOutsideScreen)
{
	int left = 50;
	int width = 100;

	bool result = false;
	
	for (int i = 0; i < 300; ++i)
	{
		--left;
		if (left + width < 0)
			result = true;
	}
	EXPECT_TRUE(result);
}

TEST(PlayerUpdateTest, playerMovementValuesChangeOnUpdateFunctions)
{
	Game game(610, 760, "testing", false);
	Player player;
	player.create(&game);

	float vStart = player.getVelY();
	player.update();
	float vEnd = player.getVelY();

	EXPECT_NE(vEnd, vStart);
}

TEST(PlayerGravityTest, playerFallsDownWhenNoPlayerInput)
{
	Game game(610, 760, "testing", false);
	Player player;
	player.create(&game);
	player.update();

	float startY = player.getY();
	float startVelY = player.getVelY();
	 
	player.update();

	EXPECT_LT(startY, player.getY());
	EXPECT_GT(player.getVelY(), startVelY);
}

TEST(PlayerJumpTest, playerVelocityRisesWhenJumping)
{
	Game game(610,760,"testing",false);
	Player player;
	player.create(&game);
	player.update();

	float startVelY = player.getVelY();

	player.jump();

	EXPECT_LT(player.getVelY(), startVelY);
}

TEST(PlayerMovement, gameOverWhenPlayerHitsGround)
{
	int height = 800;
	int width = 600;

	float x = 0, y = 0;
	float velY = 0;

	bool result = false;

	for (int i = 0; i < 500; ++i)
	{
		velY *= 0.97;

		if (velY >= -0.1 && velY <= 0.0f)
			velY = 0.0f;

		velY += 2.7f;

		y += velY;

		if (y > height)
			result = true;
	}

	EXPECT_TRUE(result);
}