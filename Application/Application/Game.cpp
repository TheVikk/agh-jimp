#include "Game.h"
#include "States.h"
#include <iostream>

Game::Game(int width = 600, int height = 800, const std::string& applicationName = "App", bool startBorderless = false)
{
	this->width = width;
	this->height = height;
	this->appName = applicationName;

	if (!startBorderless)
		window.create(sf::VideoMode(width, height), appName);
	else
		window.create(sf::VideoMode(width, height), appName, sf::Style::None); 

	window.setFramerateLimit(30);
}

Game::~Game()
{
	while (!states.empty())
	{
		popState();
	}
}

void Game::gameLoop()
{
	closing = false;

	try
	{
		while (!closing)
		{
			peekState()->update();

			window.clear();

			peekState()->render();

			window.display();
		}
	}
	catch (std::exception& ex)
	{
		std::cout << ex.what() << std::endl;
		getchar();
	}
	catch (...)
	{
		getchar();
	}

	window.close();
}

void Game::pushState(State* state)
{
	states.push_back(state);
}

void Game::popState()
{
	State* ptr = states.back();
	ptr->dispose();
	states.pop_back();
}

void Game::changeState(State* state)
{
	if (!states.empty())
		popState();

	pushState(state);
}

State* Game::peekState()
{
	if (states.empty())
		return nullptr;

	return states.back();

}

State* Game::peekSecondState()
{
	return states.at(states.size() -2);
}


int Game::getHeight()
{
	return height;
}

int Game::getWidth()
{
	return width;
}