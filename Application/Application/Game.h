#pragma once

#include <SFML\Graphics.hpp>
#include <SFML\Window.hpp>

#include <vector>

class State;

class Game {
public:
	Game(int width, int height, const std::string& applicationName, bool startBorderLess);
	~Game();

	void pushState(State* state);
	void popState();
	void changeState(State* state);
	State* peekState();
	State* peekSecondState();

	void gameLoop();

	int getWidth();
	int getHeight();

	bool closing;
	sf::RenderWindow window;

private:
	int width, height;
	std::string appName;

	std::vector<State*> states;
};