#include <iostream>
#include <conio.h>
#include "Game.h"
#include "States.h"

int main(int argc, char** argv)
{
	Game game(510, 680, "Projekt JiMP", false);

	game.pushState(new StartState(&game));

	game.gameLoop();
}