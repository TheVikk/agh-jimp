#pragma once

#include <exception>

struct imageNotLoadedException : public std::exception
{
	const char* what() const throw ()
	{
		return "Image has not been loaded correctly";
	}
};

struct fontNotLoadedException : public std::exception
{
	const char* what() const throw ()
	{
		return "Font has not been loaded correctly";
	}
};