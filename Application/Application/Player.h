#pragma once

#include <SFML/Graphics.hpp>

#include "Game.h"

class Player
{
public:
	void create(Game* game);

	void jump();
	void update();
	void draw(sf::RenderWindow& window);

	float getX();
	float getY();
	float getVelY();

	int counter;
	sf::CircleShape shape;

private:
	float x, y;
	float velY;
	float playerWidth;

	Game* game;
	sf::Texture texture;
};