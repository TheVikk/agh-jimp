#pragma once

#include <SFML\Graphics.hpp>
#include <SFML\Window.hpp>

#include <stack>

#include "Exceptions.h"
#include "Player.h"
#include "Game.h"
#include "Obstacles.h"

//--------------------------------------------------------------------------------------------------
// Base State class
//--------------------------------------------------------------------------------------------------

class State
{
public:
	virtual void render() = 0;
	virtual void update() = 0;
	virtual void proccessEvents() = 0;
	virtual void dispose() = 0;

	Game* game;
};

//--------------------------------------------------------------------------------------------------
// PlayState
//--------------------------------------------------------------------------------------------------

class PlayState : public State
{
public:
	PlayState(Game* game);
	void load();
	void dispose();

	void render() override;
	void update() override;

	void proccessEvents() override;
	void proccessEvents(bool gameOver);

	void setGameOver();

	bool gameOver = false;
	sf::Text counterText;

private:
	Player player;
	sf::Font font;
	sf::Sprite sprite;
	sf::Texture texture;
	sf::Text gameOverText;
	ObstacleManager obstacleManager;
};

//--------------------------------------------------------------------------------------------------
// PauseState
//--------------------------------------------------------------------------------------------------

class PauseState : public State
{
public:
	PauseState(Game* game);
	void load();
	void dispose();

	void render() override;
	void update() override;

	void proccessEvents() override;

private:
	sf::Font font;
	sf::Text text;
	sf::CircleShape triangle;
	sf::RectangleShape rect;
};


//--------------------------------------------------------------------------------------------------
// PauseState
//--------------------------------------------------------------------------------------------------
class StartState : public State
{
public:
	StartState(Game* game);
	void load();
	void dispose();

	void render() override;
	void update() override;

	void proccessEvents() override;

private:
	sf::Font font;
	sf::Sprite sprite;
	sf::Texture texture;
	std::vector<sf::Text> instructions;
};
