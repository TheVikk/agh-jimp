#include "Obstacles.h"
#include "States.h"

#include <cstdlib>
#include <ctime>

//--------------------------------------------------------------------------------------------------
// Obstacles
//--------------------------------------------------------------------------------------------------

template <class T>
Obstacle<T>::Obstacle(T left, T top, T height)
{
	this->init(left, top, height);
}
template <class T>
void Obstacle<T>::init(T left, T top, T height)
{
	passed = false;

	upperRect.setSize(sf::Vector2f(width, (height / 16) + rand() % (height / 2)));
	upperRect.setPosition(left, top);

	int lowerRectTop = upperRect.getGlobalBounds().top + upperRect.getGlobalBounds().height + gap;

	lowerRect.setSize(sf::Vector2f(width, height - lowerRectTop));
	lowerRect.setPosition(left, lowerRectTop);
}

template <class T>
bool Obstacle<T>::shouldReset()
{
	return (upperRect.getGlobalBounds().left + upperRect.getGlobalBounds().width < 0);
}

template <class T>
void Obstacle<T>::move()
{
	float xMove = 5.5f;

	upperRect.setPosition(sf::Vector2f(upperRect.getGlobalBounds().left - xMove, upperRect.getGlobalBounds().top));
	lowerRect.setPosition(sf::Vector2f(lowerRect.getGlobalBounds().left - xMove, lowerRect.getGlobalBounds().top));
}

template <class T>
void Obstacle<T>::render(sf::RenderWindow& window)
{
	window.draw(upperRect);
	window.draw(lowerRect);
}

template <class T>
bool Obstacle<T>::isColiding(Player& player)
{
	return rectangleColision(player, upperRect) || rectangleColision(player, lowerRect);
}

template <class T>
bool Obstacle<T>::rectangleColision(const Player& player, const sf::RectangleShape& rect)
{
	float cx = player.shape.getGlobalBounds().left + player.shape.getGlobalBounds().width/2;
	float cy = player.shape.getGlobalBounds().top + player.shape.getGlobalBounds().height/2;
	float radius = player.shape.getRadius();

	float left = rect.getGlobalBounds().left;
	float top = rect.getGlobalBounds().top;
	float right = left + rect.getGlobalBounds().width;
	float bottom = top + rect.getGlobalBounds().height;

	float closestX = (cx < left ? left : (cx > right ? right : cx));
	float closestY = (cy < top ? top : (cy > bottom ? bottom : cy));
	float dx = closestX - cx;
	float dy = closestY - cy;

	return (dx * dx + dy * dy) <= radius * radius;
}
//--------------------------------------------------------------------------------------------------
// ObstacleManager
//--------------------------------------------------------------------------------------------------

void ObstacleManager::create(Game* game)
{
	this -> game = game;
	
	srand(time(0));

	unsigned left;
	for (size_t i = 0, left = 600; i < 3; ++i, left += Obstacle<int>::spacing + Obstacle<int>::width)
	{
		obstacles.push_back(Obstacle<int>(left, 0, game->getHeight()));
	}

	if (!obstacles.at(0).texture.loadFromFile("resources/greenbot.png"))
		throw new imageNotLoadedException;
	if (!obstacles.at(1).texture.loadFromFile("resources/blackbot.png"))
		throw new imageNotLoadedException;	
	if (!obstacles.at(2).texture.loadFromFile("resources/redbot.png"))
		throw new imageNotLoadedException;

	for (auto &v : obstacles)
	{
		v.lowerRect.setTexture(&v.texture);
		v.upperRect.setTexture(&v.texture);
	}
}

void ObstacleManager::render()
{
	for (size_t i = 0; i < obstacles.size(); ++i)
	{
		obstacles.at(i).render(game->window);
	}
}

Obstacle<int>& ObstacleManager::getPredecessor(unsigned i)
{
	if (i == 0)
		return obstacles.at(obstacles.size() - 1);
	else
		return obstacles.at(-1 + i);
}

void ObstacleManager::update(Player& player)
{
	unsigned i = 0;

	for (auto &v : obstacles)
	{
		if(v.isColiding(player))
			dynamic_cast<PlayState*>(game->peekState())->setGameOver();

		if (v.shouldReset())
			v.init(getPredecessor(i).lowerRect.getGlobalBounds().left + getPredecessor(i).width + Obstacle<int>::spacing, 0, game->getHeight());

		if (!v.passed && player.getX() > v.lowerRect.getGlobalBounds().left + v.width)
		{
			++player.counter;
			dynamic_cast<PlayState*>(game->peekState())->counterText.setString(std::to_string(player.counter));
			v.passed = true;
		}

		v.move();

		++i;
	}
}

