#pragma once

#include <SFML\Graphics.hpp>
#include <SFML\Window.hpp>

#include "Player.h"
#include "Game.h"

//--------------------------------------------------------------------------------------------------
// Obstacles
//--------------------------------------------------------------------------------------------------

template <class T>
class Obstacle 
{
public:
	static const T gap = 300;
	static const T width = 75;
	static const T spacing = 200;

	Obstacle(T left, T top, T height);
	void init(T left, T top, T height);

	void move();
	void render(sf::RenderWindow& window);

	bool shouldReset();
	bool isColiding(Player& player);
	bool rectangleColision(const Player& player, const sf::RectangleShape& rect);

	bool passed;

	sf::Texture texture;
	sf::RectangleShape upperRect;
	sf::RectangleShape lowerRect;
};

//--------------------------------------------------------------------------------------------------
// ObstacleManager
//--------------------------------------------------------------------------------------------------

class ObstacleManager
{
public:
	void create(Game* game);
	void update(Player& player);
	void render();

	Obstacle<int>& getPredecessor(unsigned index);

	std::vector<Obstacle<int>> obstacles;

private:
	Game* game;
};