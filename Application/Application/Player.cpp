#include "Player.h"
#include "States.h"

#include <iostream>

void Player::create(Game* game)
{
	this->game = game;

	playerWidth = 50.0f;
	counter = 0;
	velY = 0.0f;

	x = (game->getWidth() - playerWidth)/3;
	y = (game->getHeight() - playerWidth)/3;

	if (!texture.loadFromFile("resources/bird.png"))
		throw new imageNotLoadedException;

	shape = sf::CircleShape::CircleShape(playerWidth);
	shape.setPosition(x, y);
	shape.setTexture(&texture);
}

void Player::draw(sf::RenderWindow& window)
{
	window.draw(shape);
}

void Player::update()
{
	if (y > game->getHeight())
		dynamic_cast<PlayState*>(game->peekState())->setGameOver();

	velY *= 0.97;

	if (velY >= -0.1 && velY <= 0.0f)
		velY = 0.0f;

	velY += 2.7f;
	y += velY;

	shape.setPosition(x, y);
}

void Player::jump()
{
	if (velY > -10)
		velY -= 35;
}

float Player::getX()
{
	return x;
}

float Player::getY()
{
	return y;
}

float Player::getVelY()
{
	return velY;
}