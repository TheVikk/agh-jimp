#include "States.h"
#include <iostream>
#include <ctime>
#include <cstdlib>


//--------------------------------------------------------------------------------------------------
// PlayState
//--------------------------------------------------------------------------------------------------

std::string getRandomGameOverText()
{
	srand(time(0));

	int i = rand() % 5;

	switch (i)
	{
		case 0:
			return "KOLEJNY TERMIN";
		case 1:
			return "KOLOS POPRAWKOWY";
		case 2:
			return "NIEZALICZONE";
		case 3:
			return "KONIECZNY WARUNEK";
		case 4:
			return "GAME OVER";
		default:
			return "";
	}
}

void centerTextToScreen(float width, float height, sf::Text& text) 
{
	sf::FloatRect textRect = text.getLocalBounds();
	text.setOrigin(textRect.left + textRect.width / 2.0f, textRect.top + textRect.height / 2.0f);
	text.setPosition(sf::Vector2f(width, height));
}

PlayState::PlayState(Game* game)
{
	this->game = game;

	obstacleManager.create(game);

	player.create(game);

	load();

	counterText.setFont(font);
	gameOverText.setFont(font);

	counterText.setString("0");
	gameOverText.setString(getRandomGameOverText());

	counterText.setCharacterSize(72);
	gameOverText.setCharacterSize(48);
	
	gameOverText.setFillColor(sf::Color::Color(sf::Uint8(255), sf::Uint8(0), sf::Uint8(0), sf::Uint8(235)));
	counterText.setFillColor(sf::Color::Color(sf::Uint8(255), sf::Uint8(255), sf::Uint8(255), sf::Uint8(255)));

	centerTextToScreen(game->getWidth() / 2.0f, game->getHeight() / 5.0f, counterText);
	centerTextToScreen(game->getWidth() / 2.0f, game->getHeight() / 4.0f, gameOverText);
}

void PlayState::render()
{
	game->window.draw(sprite);

	if (!gameOver)
	{
		game->window.draw(counterText);
		obstacleManager.render();
		player.draw(game->window);
	}
	else
	{
		obstacleManager.render();
		game->window.draw(counterText);
		game->window.draw(gameOverText);
	}
}

void PlayState::update()
{
	if (!gameOver)
	{
		obstacleManager.update(player);
		player.update();
		proccessEvents();
	}
	else
		proccessEvents(true);
}

void PlayState::setGameOver()
{
	gameOver = true;

	counterText.setCharacterSize(48);
	counterText.setString("You Scored: " + std::to_string(player.counter));

	centerTextToScreen(game->getWidth() / 2.0f, gameOverText.getGlobalBounds().top + gameOverText.getGlobalBounds().height + 75, counterText);
}

void PlayState::load()
{
	if (!texture.loadFromFile("resources/bg.jpg"))
		throw new imageNotLoadedException;

	if (!font.loadFromFile("resources/OPTIContact-LightAgency.otf"))
		throw new fontNotLoadedException;

	sprite.setTexture(texture);
}

void PlayState::proccessEvents()
{
	proccessEvents(false);
}

void PlayState::proccessEvents(bool gameOver)
{
	sf::Event event;

	if (!gameOver) 
	{
		while (game->window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				game->closing = true;

			else if (event.type == sf::Event::KeyPressed)
			{
				if (event.key.code == sf::Keyboard::Escape)
					game->closing = true;

				else if (event.key.code == sf::Keyboard::Space || event.key.code == sf::Keyboard::Up)
					player.jump();

				else if (event.key.code == sf::Keyboard::P)
					game->pushState(new PauseState(game));

			}
			else if (event.type == sf::Event::MouseButtonPressed)
				player.jump();
		}
	}
	else
	{
		while (game->window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				game->closing = true;

			else if (event.type == sf::Event::KeyPressed)
			{
				if (event.key.code == sf::Keyboard::Escape)
					game->closing = true;

				else if (event.key.code == sf::Keyboard::Space || event.key.code == sf::Keyboard::Up)
				{
					game->changeState(new StartState(game));
					break;
				}
			}
			else if (event.type == sf::Event::MouseButtonPressed)
			{
				game->changeState(new StartState(game));
				break;
			}
		}
	}
}

void PlayState::dispose()
{
	delete this;
}

//--------------------------------------------------------------------------------------------------
// PauseState
//--------------------------------------------------------------------------------------------------

PauseState::PauseState(Game* game)
{
	this->game = game;

	load();

	text.setFont(font);
	text.setString("Paused");
	text.setCharacterSize(48);
	text.setFillColor(sf::Color::Color(sf::Uint8(255), sf::Uint8(255), sf::Uint8(255), sf::Uint8(160)));

	centerTextToScreen(game->getWidth() / 2.0f, game->getHeight() / 4.0f, text);

	triangle = sf::CircleShape::CircleShape(70, 3);
	triangle.setFillColor(sf::Color::Color(sf::Uint8(255), sf::Uint8(255), sf::Uint8(255), sf::Uint8(160)));
	triangle.setRotation(90);
	triangle.setPosition(text.getPosition().x + (triangle.getLocalBounds().width / 2),text.getPosition().y + 60);

	rect = sf::RectangleShape::RectangleShape(sf::Vector2f(game->getWidth(), game->getHeight()));
	rect.setFillColor(sf::Color::Color(sf::Uint8(0), sf::Uint8(0), sf::Uint8(0), sf::Uint8(155)));
	rect.setPosition(0, 0);
}

void PauseState::render()
{
	game->peekSecondState()->render();

	game->window.draw(rect);
	game->window.draw(triangle);
	game->window.draw(text);
}

void PauseState::update()
{
	proccessEvents();
}

void PauseState::dispose()
{
	delete this;
}

void PauseState::proccessEvents()
{
	sf::Event event;
  	while (game->window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			game->closing = true;

		else if (event.type == sf::Event::KeyPressed)
		{
			if (event.key.code == sf::Keyboard::Escape)
				game->closing = true;

			if (event.key.code == sf::Keyboard::P)
			{
				game->popState();
				break;
			}
		}
		else if (event.type == sf::Event::MouseButtonPressed)
			game->popState();
	}
}

void PauseState::load()
{
	if (!font.loadFromFile("resources/OPTIContact-LightAgency.otf"))
		throw new fontNotLoadedException;
}

//--------------------------------------------------------------------------------------------------
// StartState
//--------------------------------------------------------------------------------------------------

StartState::StartState(Game* game)
{
	this->game = game;

	load();

	for (size_t i = 0; i < 2; ++i)
	{
		sf::Text instruction;

		instruction.setFont(font);
		instruction.setCharacterSize(32);
		instruction.setFillColor(sf::Color::Color(sf::Uint8(255), sf::Uint8(255), sf::Uint8(255), sf::Uint8(180)));

		if (i == 0)
		{
			instruction.setString("[Space] or [LPM] or [UPARROW] - JUMP");
			centerTextToScreen(game->getWidth() / 2.0f, game->getHeight() / 4.5f, instruction);
		}
		else if (i == 1)
		{
			instruction.setString("[P] - PAUSE");
			centerTextToScreen(game->getWidth() / 2.0f, game->getHeight() / 3.5f, instruction);
		}

		instructions.push_back(instruction);
	}
}

void StartState::render()
{
	game->window.draw(sprite);

	for (auto instruction : instructions)
	{
		game->window.draw(instruction);
	}
}

void StartState::update()
{
	proccessEvents();
}
void StartState::dispose()
{
	delete this;
}
void StartState::proccessEvents()
{
	sf::Event event;

	while (game->window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			game->closing = true;

		else if (event.type == sf::Event::KeyPressed)
		{
			if (event.key.code == sf::Keyboard::Escape)
				game->closing = true;

			if (event.key.code == sf::Keyboard::Space || event.key.code == sf::Keyboard::Up)
			{
				game->changeState(new PlayState(game));
				break;
			}
		}
		else if (event.type == sf::Event::MouseButtonPressed)
		{
			game->changeState(new PlayState(game));
			break;
		}
	}
}

void StartState::load()
{
	if (!font.loadFromFile("resources/OPTIContact-LightAgency.otf"))
		throw new fontNotLoadedException;

	if (!texture.loadFromFile("resources/bg.jpg"))
		throw new imageNotLoadedException;

	sprite.setTexture(texture);
}
